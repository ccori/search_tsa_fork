<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model 
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'patients';

    public $timestamps = false;

// 	public function users()
//     {
//         return $this->belongsToMany('App\Models\User', 'users_to_patients');
//     }
}
