<?php namespace App\Services;

use \App\Models\Question;
Class QuestionService
{


    public function save_question($data, $question_id = null)
    {

        if ($question_id == null) {
            $question = new Question();
        } else {
            $question = Question::find($question_id);
        }

        foreach ($data as $key => $save_data) {
            $question->$key = $data[$key];
        }
        $question->save();

        return $question->id;

    }


}