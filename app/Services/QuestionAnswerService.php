<?php namespace App\Services;

use \App\Models\QuestionAnswer;
use Illuminate\Support\Facades\DB;

Class QuestionAnswerService
{


    public function save_question_answer($data, $question_answer_id = null){

        if ($question_answer_id == null) {
            $question_answer = new QuestionAnswer();
        } else {
            $question_answer = QuestionAnswer::find($question_answer_id);
        }

        foreach ($data as $key => $save_data) {
            $question_answer->$key = $data[$key];
        }
        $question_answer->save();

        return $question_answer->id;
    }

    public function get_answers_for_question($question_id){

        $answers = QuestionAnswer::where('question_id', $question_id)->get();
        return $answers;
    }

    public function get_punctaj_for_answers(array $answers, $intrebare_importanta=null){
        $query = QuestionAnswer::join('questions', 'questions.id', '=', 'questions_answers.question_id')
            ->select(DB::raw('sum(punctaj_raspuns) as punctaj_total'))
            ->whereIn('questions_answers.id',$answers['intrebari'] );
        if($intrebare_importanta == 1){
            $query = $query -> where('intrebare_importanta', '=', 1);
        }
        $query = $query->firstOrFail();
        return $query;
    }

    public function validate_answers(){

    }
}
