@extends('layouts.app') @section('content')
<script data-require="angular-messages@*" data-semver="1.4.3" src="https://code.angularjs.org/1.4.3/angular-messages.min.js"></script>
<script type="text/javascript" src="/js/ngRemoteValidate.0.6.1.min.js"></script>
<script src="/js/user_area/create_pacient.js"></script>

@include('user_area.show_questionnaire', array('pacient_id'=>2))
<div class="container"
	 ng-app="pacient_form">
	<form id="create_form" action="/pacient/submit_questionnaire/" method="post"
		  ng-controller="validateCtrl" name="create_form"
		  novalidate>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Adauga Pacient</div>
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li> @endforeach
					</ul>
				</div>
				@endif
				<div class="panel-body">

						
						<div class="form-group">
							<label class="col-md-4 control-label">Nume</label>
							<div class="col-md-6">
								<input type="text" class="form-control input-sm" name="nume" id="nume" placeholder="Nume"
									value="{{ old('nume') }}" ng-model="nume" required ng-pattern="/^[a-zA-Z\s]*$/"> 
								<span style="color: red" ng-show="(create_form.nume.$dirty || submitted) && create_form.nume.$invalid"> 
									<span ng-show="create_form.nume.$error.required">Câmpul este obligatoriu.</span>
									<span ng-show="create_form.nume.$error.pattern">Câmpul poate contine doar litere si spatiu.</span>
								</span>
							</div>
							<label class="col-md-4 control-label">Prenume</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="prenume" placeholder="Prenume"
									value="{{ old('prenume') }}" ng-model="prenume" required ng-pattern="/^[a-zA-Z\s]*$/"> 
								<span style="color: red" ng-show="(create_form.prenume.$dirty || submitted) && create_form.prenume.$invalid"> 
									<span ng-show="create_form.prenume.$error.required">Câmpul este obligatoriu.</span>
									<span ng-show="create_form.prenume.$error.pattern">Câmpul poate contine doar litere si spatiu.</span>
								</span>
							</div>
							<label class="col-md-4 control-label">CNP</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="cnp" placeholder="CNP"
									value="{{ old('cnp') }}" ng-model="cnp" required ng-pattern="/^[0-9]*$/" ng-minlength="13" ng-maxlength="13"
									 ng-remote-method="POST"
									 ng-remote-validate='/pacient/verfy_cnp/' 
								> 
								<span style="color: red" ng-show="(create_form.cnp.$dirty || submitted) && create_form.cnp.$invalid"> 
									<div ng-messages="create_form.cnp.$error">
                                    <span ng-show="create_form.prenume.$error.required">Câmpul este obligatoriu.</span>
										<div ng-messages-include="/error_messages/default_error_messages"></div>
									    <span ng-message="pattern">Câmpul CNP poate contine doar numere.</span>
                                            <span ng-message="minlength, maxlength">Câmpul CNP trebuie sa aiba 13 caractere.</span>
									</div>
								</span>
								
								<span style="color: red" ng-show="create_form.cnp.$error.ngRemoteValidate">
							        CNP existent in baza de date
							    </span>
							</div>
							<label class="col-md-4 control-label">Nume Mama</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="mama"  placeholder="Nume Mama"
									value="{{ old('mama') }}">
							</div>
							<label class="col-md-4 control-label">Nume Tata</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="tata" placeholder="Nume Tata"
									value="{{ old('tata') }}">
							</div>
							<label class="col-md-4 control-label">Adresa</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="adresa" placeholder="Adresa"
									value="{{ old('adresa') }}" ng-model="adresa" required ng-pattern="/^[a-zA-Z\s0-9\/]*$/">
								<span style="color: red" ng-show="(create_form.adresa.$dirty || submitted) && create_form.adresa.$invalid"> 
									<span ng-show="create_form.adresa.$error.required">Câmpul este obligatoriu.</span>
									<span ng-show="create_form.adresa.$error.pattern">Câmpul poate contine doar litere si spatiu.</span>
								</span>
							</div>
							
							<label class="col-md-4 control-label">Judet</label>
							<div class="col-md-6">
								
								<select id="judet" name="judet"  ng-model="judet"  ng-change="change_oras(judet)" required class="form-control" placeholder="Judet">
								<option value="">Selecteaza judet</option>
								@foreach ($counties as $county)
								    <option value="{{ $county->id }}">{{ $county->name }}</option>
								@endforeach
								</select>
								<span style="color: red" ng-show="(create_form.judet.$dirty || submitted) && create_form.judet.$invalid"> 
									<span ng-show="create_form.judet.$error.required">Câmpul Judet este obligatoriu.</span>
								</span>
							</div>
							<label class="col-md-4 control-label">Localitate</label>
							<div class="col-md-6">
								<select id="localitate" name="localitate"  ng-model="localitate" required class="form-control">
								<option value="">Selecteaza judet intai</option>
								<option ng:repeat="town in choose_town" value="@{{town.id}}">     
                                    @{{town.name}}
                                </option>
								</select>
								<span style="color: red" ng-show="(create_form.localitate.$dirty || submitted) && create_form.localitate.$invalid"> 
									<span ng-show="create_form.localitate.$error.required">Câmpul Localitate este obligatoriu.</span>
								</span>
							</div>
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="email" placeholder="Email"
									value="{{ old('email') }}" ng-model="email" required ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z0-9]+\.[a-z0-9.]{2,5}$/"> 
								<span style="color: red" ng-show="(create_form.email.$dirty || submitted) && create_form.email.$invalid"> 
									<span ng-show="create_form.email.$error.required">Câmpul Email este obligatoriu.</span>
									<span ng-show="create_form.email.$error.pattern">Câmpul Email .</span>
								</span>
							</div>
							<label class="col-md-4 control-label">Telefon</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="telefon" placeholder="Telefon"
									value="{{ old('telefon') }}" ng-model="telefon" required> 
								<span style="color: red" ng-show="(create_form.telefon.$dirty || submitted) && create_form.telefon.$invalid"> 
									<span ng-show="create_form.telefon.$error.required">Câmpul Telefon este obligatoriu.</span>
								</span>
							</div>
							<label class="col-md-4 control-label">Observatii Medicale</label>
							<div class="col-md-6">
								<textarea id="observatii_medicale" class="form-control" name="observatii_medicale">{{ old('observatii_medicale') }}</textarea>
							</div>
							<label class="col-md-4 control-label"> </label>
							<div class="col-md-6">
								<button class="btn btn-primary btn-block"  id="show_questionnaire" type="button" ng-click="submit_first_step()">Aplica chestionar</button>
							</div>
						</div>

			</div>
		</div>
	</div>
	@yield('questionnaire')
	<input type="hidden" name="first_step" id="first_step" ng-model="first_step" value="0">
 </form>
</div>


@overwrite
