@extends('layouts.app') @section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Datele au fost trimise cu succes!</div>

                    <div class="panel-body">
                      <p>In cazul in care datele din chestionar au fost introduse de catre un medic de familie, informatiile au fost salvate, iar raspunsurile vor fi interpretate si de catre psihologii din cadrul Asociatiei Autism Baia Mare.</p>
                        
                        <p>Daca testul este pozitiv pentru autism:
                        <ul>
                        <li> acest lucru nu garanteaza ca persoana evaluata are o tulburare din spectrul autist;</li>
                        <li> este recomandata continuarea evaluarii de catre psiholog/psihiatru pediatru;</li>
                      	</ul>
                        Daca testul este negativ - copilul nu are autism.
                        </p>
                    </div>
                    <div class="center-txt alert alert-info " role="alert">
											Pentru mai multe informatii, detalii, sfaturi sau consiliere de specialitate despre autism, apelati <br/><strong>infoline Autism: 0800 500288 </strong> - numar gratuit apelabil din orice retea
										</div>
                </div>
                <div class="panel panel-default panel-body">
                    <h3 class="center-txt">Concluzia preliminara a testului: </h3>
                    <h4>{{$message}}  </h4>
                </div>
            </div>
        </div>
    </div>



@stop