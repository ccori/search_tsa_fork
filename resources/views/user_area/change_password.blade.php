@extends('layouts.app') @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Schimbă parola</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Rezolvati erorile.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (isset($message))
                        <div class="alert alert-info">
                            <ul>
                                    <li>{{ $message }}</li>
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="/user/password">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label class="col-md-4 control-label">Parola actuală</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Parola nouă</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="new_password_confirmation" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Confirmați parola nouă</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="new_password" >
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Schimbă parola
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection